#!/usr/bin/perl -w
#
# Copyright 2016 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# runs apt-update and returns detail on connectivity to the configured apt mirror 
use 5.010; 
use strict;
use warnings;

# this module will need to be installed
# see http://search.cpan.org/~dagolden/Capture-Tiny-0.30/lib/Capture/Tiny.pm
use Capture::Tiny ':all';

## PREAMBLE AND SETUP ##
#
# ensure this is being run as root
my $whoami = getpwuid($>);
if ($whoami ne "root") { print "This command needs to be run as root (use sudo)\n"; exit; }

## RUN
#
 my ($stdout, $stderr, $exit) = capture {
     system( 'apt-get update' );
   };

if ($exit > 0) {
	print "DOWN\n\n";
	print "$stderr\n";
} else {
	print "OK\n\n";
	my $result = `cat /etc/apt/sources.list`;
	print "$result\n";
}

# PRINT FOOTER
print "This created by https://github.com/hooliowobbits/aarnet-mirror-test\n";
