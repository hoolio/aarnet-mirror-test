# aarnet-mirror-test

This simple code is designed to test the functionality of the apt mirror
 at http://mirror.aarnet.edu.au/debian/ and return output
 to a txt file to be rendered by apache.  Participating hosts (noted below)
 can then be checked by a nagios instance owned by AARNET staff.

The more partipants, the better the metrics.  If you have a debian instance 
already, and can spare some minor load please do consider particpating;

## How to help
1. Please perform the setup as documented below  
2. Advise of the full URL by creating an Issue on this github repo
3. I'll add you to the list of hosts in participants.txt  

NB: Please also advise if you're leaving so you can be removed from the list

## Setup
- install debian jessie onto a host at ip x.x.x.x
- change /etc/apt/sources.list to have the following content;  
        ```deb http://mirror.aarnet.edu.au/debian/ jessie main contrib non-free```  
        ```deb http://mirror.aarnet.edu.au/debian/ jessie-updates main contrib non-free```  
        ```deb-src http://mirror.aarnet.edu.au/debian/ jessie main contrib non-free```  
        ```deb-src http://mirror.aarnet.edu.au/debian/ jessie-updates main contrib non-free```  
- install apache2
- cpan install Capture::Tiny
- clone this repo into /usr/local/aarnet-mirror-test
- run ```sudo /usr/local/aarnet-mirror-test/do.apt.update.sh``` and check it works
- choose either secure, or insecure exection below

### Less secure;
- add the following root crontab entry;  
        ```*/5 * * * * /usr/local/aarnet-mirror-test/do.apt.update.sh > /var/www/html/index.html```

### More secure;
- add a local user called aarnettest ```sudo adduser aarnettest```
- add the following line to /etc/sudoers via ```sudo visudo```  
        ```aarnettest ALL=(root) /usr/bin/apt-get```  
        ```aarnettest ALL=(root) /usr/local/aarnet-mirror-test/do.apt.update.sh```  
        ```aarnettest ALL=(root) /usr/bin/perl```  
- add the following the the aarnettest users crontab with ```sudo crontab -u aarnettest -e```   
        ```*/5 * * * * /usr/local/aarnet-mirror-test/do.apt.update.sh > /var/www/html/index.html```   
- allow aarnettest access to /var/www/html/index.html with ```sudo chmod +w aarnettest /var/www/html/index.html```   

### Test
- visit http://x.x.x.x and check the output  

## Ongoing Operation
- You can expect your host to be polled by aarnet every 5 mins or so.
- You should probably do a ```git pull``` occasionally to make sure you're running recent code

## Appendix
There's some useful info on how sources.list works at https://wiki.debian.org/SourcesList
